public Class LCBSAddCompanyCallout{
  
    public class ContractorCompanyUsers{
        String UserId;
        String Name;
        String ExternalAppUserId;
        String UserType;
      
       Public ContractorCompanyUsers( String UserId,String Name, String ExternalAppUserId, String UserType)
       { 
           this.UserId = UserId;
           this.Name = Name;
           this.ExternalAppUserId = ExternalAppUserId;
           this.UserType = UserType;  
       }
    }
    public class CompanyUsers{
        String CompanyId;
        String CompanyName;
        ContractorCompanyUsers[] LstInfo = new List<ContractorCompanyUsers>();     
        Public CompanyUsers( String CompanyId,String CompanyName,ContractorCompanyUsers[] LstInfo)
        { 
           this.CompanyId= CompanyId;
           this.CompanyName= CompanyName;
           this.LstInfo = LstInfo;         
        }
    }  
     
     public static void onAfterInsert(String userList)
     {
     integer Count= 1;
     LCBS_RequestResponse_Log__c logs = new LCBS_RequestResponse_Log__c();       
        ContractorCompanyUsers[] log = new List<ContractorCompanyUsers>();
        CompanyUsers[] vlogs = new List<CompanyUsers>();
        List<User> lstUser = [SELECT Id,Name,AccountId,ContactId,IsActive,IsPortalEnabled,ProfileId,FederationIdentifier,UserRoleId,UserType FROM User where id=:userList and IsPortalEnabled=true];
        Set<Id> vSetId = new Set<Id>();
        for(User vUsr: lstUser){
            vSetId.add(vUsr.contactid);
        }
        List<Contact> lstContact = [select id,accountid,role__c,account.name from contact where id=:vSetId];
        //List<User> lstUser = [SELECT Id,Name,AccountId,ContactId,IsActive,IsPortalEnabled,ProfileId,UserRoleId,UserType FROM User where id=:vSetId and IsPortalEnabled=true];
        /*
        for(User u1 :lstUser){          
             log.add(new ContractorCompanyUsers(u1.id,u1.name,u1.id,lstContact[0].role__c));                  
        }        
        for(User u2 :lstUser){
            for(contact vCon: lstContact){
                if(u2.contactid == vcon.id){
                    vlogs.add(new CompanyUsers(vCon.account.id,vcon.account.name,log));
                }
            }
        } 
        */
        azureactiveDirectoryHelper activeDirectoryHelper = new azureactiveDirectoryHelper();
        String authorizationHeader = activeDirectoryHelper.getLcbsWebApiAccessToken();
        system.debug('Access Token:'+authorizationHeader );
        Http http=new Http();
        HttpRequest req =new HttpRequest();
        HTTPResponse res = new HTTPResponse();      
        JSONGenerator gen = JSON.createGenerator(true);
        for(User u2 :lstUser){
            for(contact vCon: lstContact){
                if(u2.contactid == vcon.id){
                    gen.writeStartObject();        
                    gen.writeObjectField('CompanyId', vCon.account.id); 
                    gen.writeObjectField('CompanyName', vcon.account.name); 
                    gen.writeFieldName('ContractorCompanyUsers');
                    gen.writeStartArray();

                    Integer i;
                    for(i=0;i<lstUser.size();i++)
                    {
                    gen.writeStartObject();
                   // gen.writeObjectField('UserId', u2.id); 
                    //gen.writeObjectField('UserId', 'dc3660fb-7be4-4779-ae83-197b53f9af15' ); //FederationIdentifier 
                    gen.writeObjectField('UserId', u2.FederationIdentifier);
                    gen.writeObjectField('Name', u2.name);
                    gen.writeObjectField('ExternalAppUserId',u2.id);
                   // gen.writeObjectField('UserType', vCon.role__c); ContractorCompanyOwner
                    gen.writeObjectField('UserType', 'ContractorCompanyOwner'); 
                    gen.writeEndObject();
                    }
                    gen.writeEndArray();
                    gen.writeEndObject();
                }
            }
        }
        String jsonString = gen.getAsString();
        system.debug('jsonString>>>>'+jsonString);
        try{
        req.setHeader('Authorization', authorizationHeader); 
        req.setHeader('Content-Type', 'application/json');
          

        List<LCBS_API_Details__c> CalloutDetails= LCBS_API_Details__c.getall().values();
        
        if (CalloutDetails[1].CompanyUserEndpnURL__c !=null && CalloutDetails[1].CompanyUserEndpnURL__c !='') {
          req.setEndpoint(CalloutDetails[1].CompanyUserEndpnURL__c);
        }
        
        system.debug('req.setEndPoint+++'+CalloutDetails[1].CompanyUserEndpnURL__c);
        req.setMethod('POST');
        req.setCompressed(false);
        req.setBody(jsonString);
        res= http.send(req);    
        String companyResponse = res.getBody();
        system.debug('Azure Response++'+companyResponse);
        system.debug('Status of Request++'+res.getStatusCode());    
        
         Integer StatusCode = res.getStatusCode();
         LCBS_APILogs.captureLogs(jsonString,res.getBody(),res.getStatusCode());
         /*logs.Name= logs.id;
         logs.Request__c = jsonString;
         logs.Response__c = res.getBody();
         if(StatusCode ==202)
         logs.Status__c = 'Success';
         else
         logs.Status__c = 'Failed';
         insert logs; */
         }
         catch(Exception ex)
         {
         system.debug(ex.getMessage());
         if(Count<5){
                Count++;
                LCBSAddCompanyCallout.onAfterInsert(userList);
                }     
         LCBS_APILogs.captureLogs(jsonString,res.getBody(),res.getStatusCode());
        /* Integer StatusCode = res.getStatusCode();
         logs.Name= logs.id;
         logs.Request__c = jsonString;
         logs.Response__c = res.getBody();
         if(StatusCode ==202)
         logs.Status__c = 'Success';
         else
         logs.Status__c = 'Failed';
         insert logs;*/
        }
     }
     
     @Future(callout=true)
     public static void onAfterInsertFuture(String userLists){  
      userLists = userLists;
     // LCBS_ContractorInviteDelegate_scheduler.contactUserId = 
       system.debug('##Naga'+userLists);
        onAfterInsert(userLists);     
     }
}