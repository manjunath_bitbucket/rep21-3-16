public without sharing class  LCBS_BuildingInfo_Controller //implements Messaging.InboundEmailHandler
{
    public Building__c fetchinfo { get; set; }
    public boolean displayPopup {get; set;}  
    public List<Building__c> fetchbuildinfo {get;set;}
    public List<Messaging.SingleEmailMessage> mails {get;set;}
    public Building__c build {get;set;}
    public Building_Owner__c buildinfo {get;set;}
    public User u {get;set;}
    public Boolean createNewSite{get; set;}
    public string Address1 {get;set;}
    public string Address2 {get;set;}
    public string Address3 {get;set;}
    public Site__c Siteinfo { get; set; }
    Public boolean PrimaryDispacther {get;set;}
      set<id> conIds;
    public string Role1 {get;set;}
    public string Role2 {get;set;}
    public string Role3 {get;set;}
    public string Role4 {get;set;}
    public string Role5 {get;set;}
    public Contact userId1 {get;set;}
    public Contact userId2 {get;set;}
    public Contact userId3 {get;set;}
    public Contact userId4 {get;set;} 
    public Contact userId5 {get;set;}
    public string error2 {get;set;}
    public string error12 {get;set;}
    public string error22 {get;set;}
    public string error32 {get;set;}
    public string error42 {get;set;}
    public Boolean errorMsg {get;set;}  
    public String PAGE_CSS { get; set; } 
    public LCBS_BuildingInfo_Controller () 
    {
        PAGE_CSS = LCBSParameters.PAGE_CSS_URL;
        errorMsg = false;
        Siteinfo = new Site__c();
        createNewSite = false;
        build =new Building__c();
        fetchinfo = new Building__c();
        fetchbuildinfo = new List<Building__c>();
        fetchinfo.Time_Zone__c ='';
        fetchinfo.Country__c ='';
        fetchbuildinfo=[Select Id,Name,Address_1__c,Address_2__c,Address__c,Alternate_Dispatcher__c,Alternate_Technician__c,Building_Owner__c,City__c,Contractor_Company_Location__c,Country__c,
                        Dispatcher__c,Building_Owner__r.name,Email_Address__c,First_Name__c,Last_Name__c,Number_of_Devices__c,Phone_Number__c,Postal_Code__c,Site__c,State__c,Technician__c,Time_Zone__c,Service_Level__c,Title__c from Building__c];
        u = [select id,Name,Contact.AccountId,ContactId from User where id=:UserInfo.getUserId()];
        system.debug('UserId********:'+u.Contact.AccountId);
       conIds = new set<id>();
         Map<Id,User> portalUserMap = new Map<Id,User>([SELECT AccountId,ContactId,Email,FirstName,Id,IsActive,IsPortalEnabled,LastName,ProfileId,UserRoleId,UserType FROM User WHERE IsPortalEnabled = true]);
         for(user ul:portalUserMap.values())
             { 
              conIds.add(ul.ContactId);   
             }
           fetchinfo.Service_Level__c= 'Select Service Level';
           fetchinfo.Country__c= 'Select Country';
           fetchinfo.State__c = 'Select State'; 
           fetchinfo.Time_Zone__c = 'Select Time Zone' ;
    }
    

    public SelectOption[] getContractorCompanyLocOptions() 
    { 
        
            SelectOption[] getContractorCompanyLocNames= new SelectOption[]{};  
            getContractorCompanyLocNames.add(new SelectOption('','--None--'));
            for (Company_Location__c c : [select id,Name from Company_Location__c order by Name]) 
            {  
                getContractorCompanyLocNames.add(new SelectOption(c.id,c.Name));
            }
            return getContractorCompanyLocNames; 
       
    }
     
    public SelectOption[] getPrimaryDispatcherOptions() 
    { 
            SelectOption[] getPrimaryDispatcherNames= new SelectOption[]{};  
            getPrimaryDispatcherNames.add(new SelectOption('','Select'));
            for (Contact co : [select id,Name,role__c from Contact  where Id in : conIds AND AccountID=:u.Contact.AccountId AND role__c not in ('Owner','Delegate') order by name]) //role__c = 'Primary Dispatcher' and 
            {  
                if( (userId2!=null && userId2.Id == co.Id) || (userId3!=null && userId3.Id == co.Id) || (userId4!=null && userId4.Id == co.Id) ||(userId5!=null && userId5.Id == co.Id) ) {
                    continue;
                }
                getPrimaryDispatcherNames.add(new SelectOption(co.id,co.Name+' '+'('+co.Role__c+')'));
            }
            return getPrimaryDispatcherNames; 
        
    }
    
    public SelectOption[] getSeconDispatcherOptions() 
    { 
        
            SelectOption[] getSeconDispatcherNames= new SelectOption[]{};  
            getSeconDispatcherNames.add(new SelectOption('','Select'));
            for (Contact co : [select id,Name,role__c from Contact where Id in : conIds AND AccountID=:u.Contact.AccountId AND role__c not in ('Owner','Delegate')  order by name]) //role__c ='Secondary Dispatcher' and
            {  
                if( (userId1!=null && userId1.Id == co.Id) || (userId3!=null && userId3.Id == co.Id) || (userId4!=null && userId4.Id == co.Id) ||(userId5!=null && userId5.Id == co.Id) ) {
                    continue;
                }
                getSeconDispatcherNames.add(new SelectOption(co.id,co.Name+' '+'('+co.Role__c+')'));
                
            }
            return getSeconDispatcherNames; 
       
    }
   
    
    public SelectOption[] getPrimaryTechOptions() 
    { 
         SelectOption[] getPrimaryTechNames= new SelectOption[]{};  
                getPrimaryTechNames.add(new SelectOption('','Select'));
                
            for (Contact co : [select id,Name,role__c  from Contact where Id in : conIds AND AccountID=:u.Contact.AccountId AND role__c not in ('Owner','Delegate') order by name]) //role__c = 'Primary Technician' and 
            {  
                if( (userId1!=null && userId1.Id == co.Id) || (userId2!=null && userId2.Id == co.Id) || (userId4!=null && userId4.Id == co.Id) ||(userId5!=null && userId5.Id == co.Id) ) {
                    continue;
                }
                getPrimaryTechNames.add(new SelectOption(co.id,co.Name+' '+'('+co.Role__c+')'));
            }
            return getPrimaryTechNames; 
       
    }
    
    public SelectOption[] getSecondaryTechOptions() 
    { 
        
            SelectOption[] getSecondaryTechNames= new SelectOption[]{};  
                getSecondaryTechNames.add(new SelectOption('','Select'));
               
                
            for (Contact co : [select id,Name,role__c  from Contact where Id in : conIds AND AccountID=:u.Contact.AccountId AND role__c not in ('Owner','Delegate')  order by name]) //role__c ='Secondary Technician' and 
            {  
                if( (userId1!=null && userId1.Id == co.Id) || (userId2!=null && userId2.Id == co.Id) || (userId3!=null && userId3.Id == co.Id) ||(userId5!=null && userId5.Id == co.Id) ) {
                    continue;
                }
                getSecondaryTechNames.add(new SelectOption(co.id,co.Name+' '+'('+co.Role__c+')')); 
            }
            return getSecondaryTechNames; 
       
    } 
    
    public SelectOption[] getDeputyOptions() 
    { 
        
            SelectOption[] getDeputyNames= new SelectOption[]{};  
                getDeputyNames.add(new SelectOption('','Select'));
               
                
            for (Contact co : [select id,Name,role__c  from Contact where Id in : conIds AND AccountID=:u.Contact.AccountId AND role__c not in ('Owner','Delegate') order by name]) //role__c ='Secondary Technician' and 
            {  
                if( (userId1!=null && userId1.Id == co.Id) || (userId2!=null && userId2.Id == co.Id) || (userId3!=null && userId3.Id == co.Id) ||(userId4!=null && userId4.Id == co.Id) ) {
                    continue;
                }
                getDeputyNames.add(new SelectOption(co.id,co.Name+' '+'('+co.Role__c+')')); 
            }
            return getDeputyNames; 
       
    } 
    public void resetUser1()
    {
     fetchinfo.Dispatcher__c =NULL;
     userId1.id = NULL;
    }
    public void resetUser2()
    {
     fetchinfo.Alternate_Dispatcher__c=NULL;
     userId2.id = NULL;
    }
    public void resetUser3()
    {
     fetchinfo.Technician__c=NULL;
     userId3.id = NULL;
    }
    public void resetUser4()
    {
     fetchinfo.Alternate_Technician__c=NULL;
     userId4.id = NULL;
    }
    public void resetUser5()
    {
     fetchinfo.User_5__c=NULL;
     userId5.id = NULL;
    }
    
    public void getPrimaryDispatcherRole() 
    {
        Role1 = [Select Role__c From Contact Where Id =: fetchinfo.Dispatcher__c].Role__c;
    }
    public void getSecondaryDispatcherRole() 
    {
        Role2 = [Select Role__c From Contact Where Id =: fetchinfo.Alternate_Dispatcher__c].Role__c;
    }public void getPrimaryTechnicianRole() 
    {
        Role3 = [Select Role__c From Contact Where Id =: fetchinfo.Technician__c].Role__c;
    }public void getSecondaryTechnicianRole() 
    {
        Role4 = [Select Role__c From Contact Where Id =: fetchinfo.Alternate_Technician__c].Role__c;
    }public void getDeputyRole() 
    {
        Role5 = [Select Role__c From Contact Where Id =: fetchinfo.User_5__c].Role__c;
    } 
    
 /* public SelectOption[] getSiteOptions() 
    { 
            SelectOption[] getSiteNames= new SelectOption[]{};  
                getSiteNames.add(new SelectOption('','--None--'));
            for (Site__c s : [select id,Name, Contractor_Company_Location__c from Site__c order by name]) 
            {  
                getSiteNames.add(new SelectOption(s.id,s.Name));   
            }
            return getSiteNames;
       
    }

    public SelectOption[] getBuildingOwnerOptions() 
    { 
       
            SelectOption[] getOwnerNames= new SelectOption[]{};  
                getOwnerNames.add(new SelectOption('','--None--'));
            for (Building_Owner__c b : [select id,Name from Building_Owner__c order by name]) 
            {  
                getOwnerNames.add(new SelectOption(b.id,b.Name));
            }
            return getOwnerNames; 
       
    }
    
    public void getContactDetails()
    {
        buildinfo = [Select Id, Name, Email_Address__c,First_Name__c,Last_Name__c,Title__c,Phone_Number__c from Building_Owner__c where ID=:fetchinfo.Building_Owner__c];
    }
    
      public void getSiteName()
    {
        if(fetchinfo.Site__c==null)
        {
            createNewSite = false; 
                  
        }
        
        else
        {
            createNewSite = true;        
        }
    }
    
    
    */
   
  
    
    
    public PageReference save() 
    {                  
          /*  if(createNewSite == false)
            {
              insert Siteinfo;
              fetchinfo.Site__c = Siteinfo.Id; 
            }  */          
           try{ 
           /*   if(Address1!=NULL)
            {
             Address1 = Address1+', ';
            }
            if(Address2!=NULL && Address3!=NULL)
            {
             Address2 = Address2+', ';
            }
            */
                      
            //fetchinfo.Address__c = Address1+', '+Address2+','+Address3;
            if(fetchinfo.Dispatcher__c == null){
            PrimaryDispacther  = true;
            }
            List<contact> conId = [select id,name,AccountId,Role__c,Firstname,lastname from contact where AccountId =: u.Contact.AccountId  and ( role__c = 'owner'OR role__c = 'Delegate')];
          //  if(conId.size()>0){
           
            //fetchinfo.Contractor_owner__c = u.ContactId;
           fetchinfo.ContractorCompany__c =  u.Contact.AccountId; 
          //}
            insert fetchinfo;
            system.debug('BuildingId******'+fetchinfo.ID);
            
            //Contractor Owner / Delegate creation
            list<LCBS_building_contractor__c> AllBuldCont =  new list<LCBS_building_contractor__c> ();
            for(contact con:conId)
            {   
                LCBS_building_contractor__c buldCont = new LCBS_building_contractor__c();
                buldCont.Building__c = fetchinfo.ID;
                buldCont.Contractor_owner__c=con.Id;
                buldCont.name = con.name;
                buldCont.First_Name__c = con.Firstname;
                buldCont.Last_Name__c= con.Lastname;
                buldCont.Role__c= con.Role__c;
                buldCont.Account_Name__c = con.AccountId;
                AllBuldCont.add(buldCont);
            }          
            if(AllBuldCont.size()>0)
            {
             insert AllBuldCont;
            }
            /*
            LCBS_building_contractor__c buldCont = new LCBS_building_contractor__c();
            buldCont.Building__c = fetchinfo.ID;
            buldCont.Contractor_owner__c=conId[0].Id;
            buldCont.name = conId[0].name;
            buldCont.First_Name__c = conId[0].Firstname;
            buldCont.Last_Name__c= conId[0].Lastname;
            buldCont.Role__c= conId[0].Role__c;
            buldCont.Account_Name__c = conId[0].AccountId;
            insert buldCont;
            */
            
            Building__c CreatedBuilding = new Building__c();
            if(fetchinfo.id != Null){
            CreatedBuilding = [Select Id,Name,Address_1__c,Address_2__c,Address__c,Alternate_Dispatcher__c,Alternate_Technician__c,Building_Owner__c,City__c,Contractor_Company_Location__c,Country__c,
                        Dispatcher__c,Building_Owner__r.name,Email_Address__c,First_Name__c,Last_Name__c,Number_of_Devices__c,Phone_Number__c,Postal_Code__c,Site__c,State__c,Technician__c,Time_Zone__c,Service_Level__c,Title__c,User_5__c,Dispatcher__r.Email,Alternate_Dispatcher__r.Email,Technician__r.Email,Alternate_Technician__r.Email,User_5__r.Email from Building__c where id =: fetchinfo.id];
            }
            system.debug('DispatcherEmail******'+CreatedBuilding.Dispatcher__r.Email);
             system.debug('AlternateDispatcher******'+CreatedBuilding.Alternate_Dispatcher__r.Email);
              system.debug('Technician******'+CreatedBuilding.Technician__r.Email);
               system.debug('AlternateTechnician*****'+CreatedBuilding.Alternate_Technician__r.Email);
                system.debug('UserEmail******'+CreatedBuilding.User_5__r.Email);
            
            List<String> emailRecipients = new List<String>();
            
            if(CreatedBuilding.Dispatcher__r.Email != NULL){
            system.debug('distpatcher contactId^^^^^^^'+fetchinfo.Dispatcher__c);
            Contact c1 = [select id,Email from contact where id=:fetchinfo.Dispatcher__c];
            system.debug('contact******:'+c1.email);
            emailRecipients.add(c1.Email);
            system.debug('Email Recipients^^^^^^'+emailRecipients);
            }
            if(CreatedBuilding.Alternate_Dispatcher__r.Email != NULL)
            {
            Contact c2 = [select id,Email from contact where id=:fetchinfo.Alternate_Dispatcher__c];
            emailRecipients.add(c2.Email);
            }
            if(CreatedBuilding.Technician__r.Email != NULL)
            {
            Contact c3 = [select id,Email from contact where id=:fetchinfo.Technician__c];
            emailRecipients.add(c3.Email);
            }
            if(CreatedBuilding.Alternate_Technician__r.Email != NULL)
            {
            Contact c4 = [select id,Email from contact where id=:fetchinfo.Alternate_Technician__c];
            emailRecipients.add(c4.Email);
            }
            if(CreatedBuilding.User_5__r.Email != NULL)
            {
            Contact c5 = [select id,Email from contact where id=:fetchinfo.User_5__c];
            emailRecipients.add(c5.Email);
            }
            system.debug('%%%%%Email Recipients'+emailRecipients.size());
            String EmailMessage = 'This email is to inform that you have been assigned as an User to the building '+fetchinfo.Name;
            //if(emailRecipients.size() > 0 || emailRecipients.size() != null  )
            if(emailRecipients.size() > 0 || (!emailRecipients.isEmpty()) )
            {
            system.debug('Test Chiran:::');
             LCBSSendEmail.Email(emailRecipients,'Building has been assigned to you',fetchinfo.Name,EmailMessage);
            /*
            mails = new List<Messaging.SingleEmailMessage>();
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
            mail.setToAddresses(emailRecipients);
            mail.setSubject('Building has been assigned to you');
            mail.setSenderDisplayName(fetchinfo.Name);
            mail.setHTMLBody(EmailMessage);
    
            mails.add(mail);
            system.debug('mails ########:'+mails);
            if(mails.size()>0){
            Messaging.sendEmail(mails);
            }
            */
            }
            //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Already this site is existed, Please Create a New Site.'));         
            /*
            EmailTemplate myEMailTemplate = [Select id,name from EmailTemplate where Name ='LCBS_BuildingAccessReqTemplate']; 
            build =[Select Id,Owner.Email,Building_Owner__r.name,Building_Owner__r.Email_Address__c,Building_Owner__r.Phone_Number__c from Building__c where id=:fetchinfo.id];
            system.debug(' fetchinfo ID*******'+fetchinfo.id);  
            system.debug('Building ID*******'+build.Id);           
            //Mail Functionality added by Nagarajan
            mails = new List<Messaging.SingleEmailMessage>();
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
           
            Contact c = [select id, Email from Contact where email <> null limit 1];
            mail.setTemplateId(myEMailTemplate.id);
            List<String> sendTo = new List<String>();
            sendTo.add(build.Building_Owner__r.Email_Address__c);
            mail.setToAddresses(sendTo);
            mail.setTargetObjectId(c.id);           
            mail.setSenderDisplayName('LCBS Building Approval');
            mail.setReplyTo('approval_for_building@1lpcig5s4p7ux2ltna158pjo0mknx7t77fw3y7pndbub6bpijt.g-3nq6jeac.cs17.apex.sandbox.salesforce.com');
            string emailMessage ='Your heating and cooling contractor is requesting data access to your building'+ ' '+fetchinfo.Name+'.  To grant them access, please reply "Approved" to this mail. To reject them access, please reply "Reject" to this mail <br/><br/><br/> Thank you.';
            mail.whatid = fetchinfo.id;
            mails.add(mail);
           
            Savepoint sp = Database.setSavepoint();
            Messaging.sendEmail(mails);
            Database.rollback(sp);
             
            List<Messaging.SingleEmailMessage> lstMsgsToSend = new List<Messaging.SingleEmailMessage>();
             for (Messaging.SingleEmailMessage email : mails) {
             Messaging.SingleEmailMessage emailToSend = new Messaging.SingleEmailMessage();
             emailToSend.setToAddresses(email.getToAddresses());
             emailToSend.setPlainTextBody(email.getPlainTextBody());
             emailToSend.setHTMLBody(email.getHTMLBody());
             emailToSend.setSubject(email.getSubject());
             lstMsgsToSend.add(emailToSend);
             }
            Messaging.sendEmail(lstMsgsToSend);
            
            Task task = new Task();
            task.WhatId = fetchinfo.id;
            task.WhoId = fetchinfo.Technician__c;
            task.Subject = 'Email Sent to Building Owner';
            task.status = 'Completed';
            task.Description ='TIME :   '+system.now() + '\r\n'+ '\r\n' + 'APPROVER DETAILS' + '\r\n' + 'NAME :  ' + build.Building_Owner__r.name +'\r\n'+ 'EMAIL :  ' + build.Building_Owner__r.Email_Address__c + '\r\n' + ' PHONE NUMBER :   ' + build.Building_Owner__r.Phone_Number__c;
            task.ActivityDate = Date.today();
            insert task;  
            */          
           // if(fetchinfo.Building_Owner__c == NULL)
           // {
            // PageReference p = new pageReference('/LCBS_MoreInfo_BuildInformation?Id='+fetchinfo.id);
             PageReference p = new pageReference('/lcbs_moreinfo_buildinfo?Id='+fetchinfo.id);
             p.setRedirect(true);
             return p;
         //   }
          /*  else
            {
             PageReference p1 = new pageReference('/LCBS_MoreInfo_BuildInfo?Id='+fetchinfo.id);
             p1.setRedirect(true);
             return p1;
            } */
             
        }
        catch (DmlException e)  {  ApexPages.addMessages(e); }       
        return null; 
    }    

    public PageReference cancel()
    {       
        //return Page.LCBS_AllBuildings;
        PageReference p = new PageReference(LCBSParameters.DISTRIBUTOR_HOME_URL);
        p.setRedirect(true);
        return p;
    }   
    public void closePopup() {        
        displayPopup = false;    
    }     
    public void showPopup() {        
        displayPopup = true;    
    }     
    public SelectOption[] getContractorCompanyLocation() 
    { 
       
            SelectOption[] getCompanyLocations= new SelectOption[]{};  
                getCompanyLocations.add(new SelectOption('','--None--'));
            for (Company_Location__c b : [select id,Name from Company_Location__c order by name]) 
            {  
                getCompanyLocations.add(new SelectOption(b.id,b.Name));
            }
            return getCompanyLocations; 
       
    }      
    
     public SelectOption[] getRole() 
    {       
        SelectOption[] getRoleNames= new SelectOption[]{};        
        getRoleNames.add(new SelectOption('','--None--'));        
         Schema.DescribeFieldResult fieldResult =Contact.Role__c.getDescribe();
       List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
       for( Schema.PicklistEntry f : ple)
       {    
          getRoleNames.add(new SelectOption(f.getLabel(), f.getValue()));
       }    
                                
        return getRoleNames;         
    }    
           
    public PageReference savePopClose() 
    {
        try 
        {
            List<Site__c> siteList= new List<Site__c>();            
            siteList.add(Siteinfo);           
            system.debug('Chiran Test:'+Siteinfo);
            insert siteList;       
        }
        catch (DmlException e)  { ApexPages.addMessages(e); }
           
        return new PageReference('/apex/LCBS_BuildingInformation').setRedirect(true); 
    }
    
    public PageReference cancelPop()
    {        
        return Page.LCBS_BuildingInformation;
    }  
    public void getUserId1()
    {
     if(fetchinfo.Dispatcher__c !=NULL )
     userId1 = [Select id,name from Contact where id=:fetchinfo.Dispatcher__c];
     else
     userId1=NULL;
    }
    public void getUserId2()
    {
     if(fetchinfo.Alternate_Dispatcher__c!=NULL )
     userId2 = [Select id,name from Contact where id=:fetchinfo.Alternate_Dispatcher__c];
     else
     userId2=NULL;
    }
    public void getUserId3()
    {
     if(fetchinfo.Technician__c!=NULL )
     userId3 = [Select id,name from Contact where id=:fetchinfo.Technician__c];
     else
     userId3=NULL;
    }
    public void getUserId4()
    {
     if(fetchinfo.Alternate_Technician__c!=NULL )
     userId4 = [Select id,name from Contact where id=:fetchinfo.Alternate_Technician__c];
     else
     userId4=NULL;
    }
    public void getUserId5()
    {
     if(fetchinfo.User_5__c!=NULL )
     userId5 = [Select id,name from Contact where id=:fetchinfo.User_5__c];
     else
     userId5=NULL;
    }
    public void errorUpdate()
    {
     errorMsg = true;
     ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please correct the errors below');
     ApexPages.addMessage(myMsg);
     system.debug('Error Test'+error2);
    }
    public void errorClear()
    {
      errorMsg = false;
    }
}