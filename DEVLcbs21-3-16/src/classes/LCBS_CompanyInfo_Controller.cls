public  class LCBS_CompanyInfo_Controller {

    


  
    public String tabOpt { get; set; }
    
    public Contact fetchUserinfo { get; set; }
    public Account acct {get; set;}
    public Contact Cont{get;set;} 
    public Account AccountInfo{get; set;}   
    Public User user{get;set;} 
    public Company_Location__c contractor {get;set;}
    public string accountId{get;set;}
    public List<Contact> con{get;set;}
    public String ContactId {get;set;}
    public Contact co {get;set;}
   Public Boolean CompanyEditPanel {get;set;} 
    Public Boolean AddUserPanel {get;set;} 
    Public Boolean EditUserPanel{get;set;}
    Public Boolean EditPanel {get;set;}
    public Account a {get;set;}
    public String userFirstName {get;set;}
    public String userLastName {get;set;}
    public String userEmail {get;set;}
    public String userPhone {get;set;}
    public String selectedRole {get;set;}
    
    Public List<Contact> conList {get;set;}
    Public Id UserAcctId {get;set;}
    
    //public Boolean DepRole {set;get;}
    //document doc{get;set;}
    //Public String Filename{get;set;}                   
    // Public Blob Filebody{get;set;}
      public String PAGE_CSS { get; set; } 
    public LCBS_CompanyInfo_Controller(){
       // PAGE_CSS = LCBSParameters.PAGE_CSS_URL;
       
        
        fetchUserinfo = new Contact(); 
        con = new List<Contact>();
        
        List<Account> acc = new List<Account>();
        co = new Contact();
        
        
        
        user =[select id,Contactid,name,Contact.id,Account.Id,Contact.Role__c,Contact.FirstName,Contact.LastName,Contact.Email from User where id=:userinfo.getuserId()]; 
        UserAcctId = user.Account.Id;
        //a = [select id,(select id,firstName,lastname,Contractor_Company_Location__c,Role__c,Email,Phone from contacts  where role__c != 'Occupant' order by lastname),EnvironmentalAccountType__c from Account where id=:user.Account.Id];
        contractor = new Company_Location__c(Company_Name__c = user.AccountId); 
        //contact = [select id,firstName,lastname,Contractor_Company_Location__c,Role__c,Email,Phone from Contact where id=:a.contacts];  
        //con.addall(a.contacts); 
       sortMethod();        
        try{
            // acct = [Select id,name,phone,email__c,State__c,Country__c,OwnerId from Account where Id =: user.AccountId];   
            acct = [Select id,name,phone,email__c,City__c,Zip__c,Address__c,Address_2__c,State__c,Country__c,shippingStreet,shippingCity,shippingCountry,shippingState,shippingPostalCode,Time_Zone__c,(Select Name,City__c,Email_Address__c,Phone__c,Address__c,State__c,Time_Zone__c,Postal_Code__c,Owner__c,Owner_Last_Name__c from Company_Locations__r),(select FirstName,LastName from Contacts),OwnerId from Account where Id =: user.AccountId];      
            Cont = [select id,FirstName,LastName,Email,Phone,Role__c,MailingStreet,MailingCity,MailingState,MailingCountry,Time_Zone__c,recordtype.name from Contact where id=:user.Contactid];
            contractor =[select id,Address__c,name,City__c,Country__c,Company_Name__c,Email_Address__c,Owner__c,Phone__c,State__c,Postal_Code__c,Time_Zone__c from Company_Location__c where id=:user.AccountId];
            
            //  SystemadminId = [select id from user where profile = ''];
            
        }
        
        
        catch(exception e){}
        
        system.debug('###ACCOUNTS'+acct);
        system.debug('###User ID'+userinfo.getuserId());
        system.debug('###User List '+Cont.FirstName);   
        system.debug('role*****'+Cont.Role__c);
        
        
        if(Cont.Role__c == 'Owner' || Cont.Role__c == 'Delegate' ){                
            CompanyEditPanel = true;  
            AddUserPanel = true; 
            EditUserPanel = true;
            EditPanel = true;
        }
        
        if(Cont.Role__c == 'Employee' || Cont.Role__c == 'Occupant' ){
            CompanyEditPanel = false;  
            AddUserPanel =false;  
            EditUserPanel = false; 
            EditPanel = false;   
        }
        
    }
    
  
    Public string selectedField {get;set;}
    Public void sortMethod(){
    con=New List<Contact>();
      if(selectedField == 'NameAsc'){
         a = [select id,(select id,firstName,lastname,Contractor_Company_Location__c,Role__c,Email,Phone from contacts  where role__c != 'Occupant' order by lastname asc),EnvironmentalAccountType__c from Account where id=:user.Account.Id];
        
      }else if(selectedField == 'NameDsc'){
         a = [select id,(select id,firstName,lastname,Contractor_Company_Location__c,Role__c,Email,Phone from contacts  where role__c != 'Occupant' order by lastname desc),EnvironmentalAccountType__c from Account where id=:user.Account.Id];
        
      } else if(selectedField == 'RoleAsc'){
         a = [select id,(select id,firstName,lastname,Contractor_Company_Location__c,Role__c,Email,Phone from contacts  where role__c != 'Occupant' and role__c !='Distributor Delegate' order by Role__c asc),EnvironmentalAccountType__c from Account where id=:user.Account.Id];
        
      }
      else if(selectedField == 'RoleDsc'){
         a = [select id,(select id,firstName,lastname,Contractor_Company_Location__c,Role__c,Email,Phone from contacts  where role__c != 'Occupant' and role__c !='Distributor Delegate' order by Role__c desc),EnvironmentalAccountType__c from Account where id=:user.Account.Id];
    
      }
      else{
       a = [select id,(select id,firstName,lastname,Contractor_Company_Location__c,Role__c,Email,Phone from contacts  where role__c != 'Occupant' order by lastname asc),EnvironmentalAccountType__c from Account where id=:user.Account.Id];
    
      }
      con.addall(a.contacts);     
      system.debug('Chiran list::::'+con.size()+'=====>>>'+con);
     // else if(selectedField == 'Role')
        //accList = [select name,accountnumber,annualrevenue from account where id IN:accList order by Annualrevenue desc limit 100 ];
    }
    Public list<SelectOption> getSortOptions(){
        list<SelectOption> sortValues =  new list<SelectOption>();
        sortValues.add(new SelectOption('NameAsc','Last Name(A-Z)'));
        sortValues.add(new SelectOption('NameDsc','Last Name(Z-A)'));
        sortValues.add(new SelectOption('RoleAsc','Role(A-Z)'));
        sortValues.add(new SelectOption('RoleDsc','Role(Z-A)'));
    return sortValues;
    }
    
    
    
     public pagereference addUsers() {
        try{
           //fetchUserinfo = new Contact(); 
            system.debug('%%%%Add User');
            if(a.EnvironmentalAccountType__c == 'Distributor')
            {
            RecordType rec = [select id,Name from RecordType where Name='ECC:Distributor Contacts' limit 1];
            System.debug('%%%%Record Type Id and Name%%%%'+rec.id+' '+rec.Name);
            fetchUserinfo.RecordTypeId = rec.id;
            }
            if(a.EnvironmentalAccountType__c == 'Contractor')
            {
            RecordType rec1 = [select id,Name from RecordType where Name='ECC:Contractor Contacts' limit 1];
            System.debug('%%%%Record Type Id and Name%%%%'+rec1.id+' '+rec1.Name);
            fetchUserinfo.RecordTypeId = rec1.id;
            } 
            //fetchUserinfo.LastName = 'hi';
            
            fetchUserinfo.AccountId =  user.Account.Id;
            fetchUserinfo.is_Invited__c = true ;
            
            String CommunityId = Network.getNetworkId();
            System.debug('CommunityId:::'+CommunityId);
            
            String CommunityUrl = Network.getLoginUrl(CommunityId);
            System.debug('CommunityUrl:::'+CommunityUrl);
            
            String Env = CommunityUrl.remove('https://').substringBefore('-');
            System.debug('Environment:::'+Env);
           
            LCBS_NewUser_Owner__c homepage = LCBS_NewUser_Owner__c.getValues(Env);
            System.debug('homepage:::'+homepage);
             System.debug('%%%%LastName'+userLastName);
            fetchUserinfo.LastName = userLastName;
            fetchUserinfo.FirstName = userFirstName; 
            fetchUserinfo.Email =userEmail; 
           // fetchUserinfo.Phone =userPhone; 
            fetchUserinfo.OwnerId = homepage.OwnerId__c;
            
            fetchUserinfo.role__c =selectedRole;
            system.debug('%%Owner Id%%'+homepage.OwnerId__c);
            
            
            //Savepoint sp1 = Database.setSavepoint();
           // List<Contact> employee = new List<Contact>();
           // employee .add(new Contact());
           // List<Database.SaveResult> srs = Database.insert(employee,true);
           
            
            //fetchUserinfo.Contractor_EULA_Accepted__c =True;
            //fetchUserinfo.Date_EULA_Accepted__c =system.today();
            insert fetchUserinfo;
    
            
            addUserApi(fetchUserinfo.id,fetchUserinfo.role__c,fetchUserinfo.Email,user.contactid);
            }
             catch(Exception e)
             {
                ApexPages.addMessages(e); 
             }
             PageReference pr;
            string url = system.label.LCBS_Navigation_Menu_Domain ;
           //Contact contactType = [select id,account.EnvironmentalAccountType__c from contact where id=:contactId];
             if(a.EnvironmentalAccountType__c == 'Distributor'){
            pr = new PageReference(url+'LCBS_Distributor_CompanyInfo');
            }
             if(a.EnvironmentalAccountType__c == 'Contractor'){
             pr = new PageReference(url+'LCBS_CompanyInfo');
            }
            return pr;
          }
          
  
       @future(callout=true)   
       public static void addUserApi(Id contactId,String contactRole,String ContactEmail, String SenderContactId)
       {
        try
        {   
        LCBSAddUserCallout.onAfterEulaAccept(contactId);
        LCBS_ContractorInviteDelegate_Controller.distributor = SenderContactId; //Inviting Contractor for the delegate/employee
        LCBS_ContractorInviteDelegate_Controller.contractor = contactId;
        // Logic is to send the email after user is saved 
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        List<String> sendTo = new List<String>();
        sendTo.add(ContactEmail);
        mail.setToAddresses(sendTo);
        mail.setSenderDisplayName('admin@lcbs.honeywell.com');
        mail.setTargetObjectId(contactId);
        EmailTemplate etEmployee = [Select Id from EmailTemplate where Name = 'LCBS_Contractor_Add_Employee'];
        EmailTemplate etDelegate = [Select Id from EmailTemplate where Name = 'LCBS_Contractor_Add_Delegate'];
        if(contactRole == 'Employee' || contactRole == 'Technician')
        {
        mail.setTemplateId(etEmployee.Id);
        }
        else if(contactRole == 'Delegate')
        {
        mail.setTemplateId(etDelegate.Id);
        }
        mails.add(mail);
        if (!Test.isRunningTest()){
        Savepoint sp = Database.setSavepoint();
            Messaging.sendEmail(mails);
           Database.rollback(sp);
             
            List<Messaging.SingleEmailMessage> lstMsgsToSend = new List<Messaging.SingleEmailMessage>();
             for (Messaging.SingleEmailMessage email : mails) {
             Messaging.SingleEmailMessage emailToSend = new Messaging.SingleEmailMessage();
             emailToSend.setToAddresses(email.getToAddresses());
             emailToSend.setPlainTextBody(email.getPlainTextBody());
             emailToSend.setHTMLBody(email.getHTMLBody());
             emailToSend.setSubject(email.getSubject());
             lstMsgsToSend.add(emailToSend);
             }
            Messaging.sendEmail(lstMsgsToSend);
           }
       }
   catch(Exception e)
    {
        system.debug(e); 
    }
  }
       public PageReference cancelupdateUser() {
        //return null;
        PageReference pr;
        string url = system.label.LCBS_Navigation_Menu_Domain ;
         if(a.EnvironmentalAccountType__c == 'Distributor'){
        pr = new PageReference(url+'LCBS_Distributor_CompanyInfo');
        }
         if(a.EnvironmentalAccountType__c == 'Contractor'){
         pr = new PageReference(url+'LCBS_CompanyInfo');
        }
        return pr;
    }
    
    
    public PageReference updateUser() {
        try{
            
            /* if( Cont.Role__c == 'Deputy' ){
if(co.Role__c == 'Occupant'){
system.debug('@@@@@@@@'+co.Role__c );
DepRole = true;     
system.debug('@@@@@@@@'+DepRole);          
return null;
} else{DepRole = false;}
}*/
      
            update co;
            system.debug('@@@@@@@@afterUPdate>>>'+co.id);
        }
        catch(Exception e)
        {
            ApexPages.addMessages(e); 
        }
        PageReference pr;
        string url = system.label.LCBS_Navigation_Menu_Domain ;
         if(a.EnvironmentalAccountType__c == 'Distributor'){
        pr = new PageReference(url+'LCBS_Distributor_CompanyInfo');
        }
         if(a.EnvironmentalAccountType__c == 'Contractor'){
         pr = new PageReference(url+'LCBS_CompanyInfo');
        }
        return pr;
        
    }
    
    
    public PageReference canceladdUser() {
        //return null;
        PageReference pr;
        string url = system.label.LCBS_Navigation_Menu_Domain ;
         if(a.EnvironmentalAccountType__c == 'Distributor'){
        pr = new PageReference(url+'LCBS_Distributor_CompanyInfo');
        }
         if(a.EnvironmentalAccountType__c == 'Contractor'){
         pr = new PageReference(url+'LCBS_CompanyInfo');
        }
        return pr;
    }
     
   
    
    public SelectOption[] getCompanyOwners() 
    { 
        
        SelectOption[] getCompanyNames= new SelectOption[]{};  
            getCompanyNames.add(new SelectOption('','--None--'));
        for (Contact a : [select id,Name from contact limit 10]) 
        {  
            getCompanyNames.add(new SelectOption(a.id,a.name)); 
        }
        return getCompanyNames; 
        
    }  
    public void getContactInfo()
    {
        system.debug('Test:::::::::'+ContactId);
        co = [select FirstName,LastName,Email,Phone,Role__c,MailingStreet,MailingCity,MailingState,MailingCountry,Time_Zone__c from contact where id=:ContactId];
        system.debug('ContactInfo******'+co.id);
    }
    public Document document {
        get {
            if (document == null)
                document = new Document();
            return document;
        }
        set;
    }
    public PageReference upload() {
        
        document.AuthorId = UserInfo.getUserId();
        document.FolderId = '00lj0000000y9f4AAA'; // put it in running user's folder
        
        try {
            document.name = user.name+'logo';
            document.IsPublic = True;
            insert document;
        } catch (DMLException e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading file'));
            return null;
        } finally {
            document.body = null; // clears the viewstate
            document = new Document();
        }
        
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'File uploaded successfully'));
        return null;
    }
    public PageReference updateCompany() {
        system.debug('Entering Method');
        // att = new Attachment(); document
        // att.parentId = user.Account.Id;
        //  att.name=String.Valueof(Filename);
        // att.body = Filebody;
        // document.name =  'Honeywell Logo';
        //  document.AuthorId = UserInfo.getUserId();
        //  document.FolderId = UserInfo.getUserId();
        //  document.IsPublic = true;
        
        
        try{
            if(cont.LastName != Null){
                //if( att.name== null || att.body == null){
                system.debug('Test Update'+cont.LastName);
                Update acct;
                update cont;
                // insert document;
                system.debug('Test Update'+acct.Phone);
                // }
            }   
        }
        catch(DmlException e){
            String error = e.getMessage();
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,error));
        }
        
        // return null;
        PageReference pr;
        string url = system.label.LCBS_Navigation_Menu_Domain ;
         if(a.EnvironmentalAccountType__c == 'Distributor'){
        pr = new PageReference(url+'LCBS_Distributor_CompanyInfo');
        }
         if(a.EnvironmentalAccountType__c == 'Contractor'){
         pr = new PageReference(url+'LCBS_CompanyInfo');
        }
        return pr;
    }
    
    public PageReference cancelCompany() {
        PageReference pr;
        string url = system.label.LCBS_Navigation_Menu_Domain ;
         if(a.EnvironmentalAccountType__c == 'Distributor'){
        pr = new PageReference(url+'LCBS_Distributor_CompanyInfo');
        }
         if(a.EnvironmentalAccountType__c == 'Contractor'){
         pr = new PageReference(url+'LCBS_CompanyInfo');
        }
        return pr;
    }
    public PageReference updateCompanyLocation() {
        
        try{
            //contractor.Company_Name__c = user.AccountId;
            insert contractor;
            
        }
        catch(DmlException e){
            String error = e.getMessage();
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,error));
        }
        //return null;
       PageReference pr;
        string url = system.label.LCBS_Navigation_Menu_Domain ;
         if(a.EnvironmentalAccountType__c == 'Distributor'){
        pr = new PageReference(url+'LCBS_Distributor_CompanyInfo');
        }
         if(a.EnvironmentalAccountType__c == 'Contractor'){
         pr = new PageReference(url+'LCBS_CompanyInfo');
        }
        return pr;
    }
    // public string selectedRole{get;set;}
    public SelectOption[] getRole() 
    {     
        SelectOption[] getRoleNames= new SelectOption[]{};    
            
           // if(Cont.Role__c == 'Owner' || Cont.Role__c == 'User' || Cont.Role__c == 'Occupant'  ){                
                  getRoleNames.add(new SelectOption('Employee','Employee')); 
                  getRoleNames.add(new SelectOption('Delegate','Delegate')); 
              //  getRoleNames.add(new SelectOption('Owner','Owner'));               
               // getRoleNames.add(new SelectOption('Occupant','Occupant')); 
          //  }  
       /* if(Cont.Role__c == 'Deputy' ){                
            getRoleNames.add(new SelectOption('User','User'));  
            getRoleNames.add(new SelectOption('Deputy','Deputy')); 
            getRoleNames.add(new SelectOption('Occupant','Occupant')); 
        }  
        */
        /* Schema.DescribeFieldResult fieldResult =Contact.Role__c.getDescribe();
List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

for( Schema.PicklistEntry f : ple)
{
getRoleNames.add(new SelectOption(f.getLabel(), f.getValue()));
}     
*/                  
        return getRoleNames;         
    }
    
     public SelectOption[] getDistRole() 
    {     
        SelectOption[] getRoleNames= new SelectOption[]{};    
         //list<SelectOption> getRoleNames  =  new list<SelectOption>();
                                  
                getRoleNames.add(new SelectOption('Delegate','Delegate')); 
              //  getRoleNames.add(new SelectOption('Owner','Owner'));  
              
        return getRoleNames;         
    }
    
    
    
    public SelectOption[] getTimeZone() 
    {       
        SelectOption[] getTimeZones = new SelectOption[]{};        
            getTimeZones.add(new SelectOption('','--None--'));          
        Schema.DescribeFieldResult fieldResult =Contact.Time_Zone__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for( Schema.PicklistEntry g : ple)
        {
            getTimeZones.add(new SelectOption(g.getLabel(), g.getValue()));
        }                            
        return getTimeZones;         
    }
    
    public String getFileId() {
        String fileId = '';
        List<Document> attachedFiles = [select name,AuthorId from Document where name = 'Honeywell-logo1'];
        if( attachedFiles != null && attachedFiles.size() > 0 ) {
            fileId = attachedFiles[0].Id;
        }
        return fileId;    
    }
    
    
}