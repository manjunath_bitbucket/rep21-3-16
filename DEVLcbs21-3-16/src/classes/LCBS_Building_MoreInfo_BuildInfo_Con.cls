public class LCBS_Building_MoreInfo_BuildInfo_Con{
public string buildingId {get;set;}
public Building__c b{get;set;}
public Building_Owner__c build {get;set;}
public Boolean buildingowner {get;set;}
public String PAGE_CSS { get; set; }
public String buildingOwnerStatus {get;set;}
Public boolean addBuildPanel {get;set;} 
Public boolean buildOwnerStatusPanel{get;set;} 
public user c {get;set;}
public user u {get;set;}
public user currentuser{get;set;}
public transient List<Messaging.SingleEmailMessage> mails {get;set;}
public boolean isDistributor {
get {
 u = [select id,Contactid,AccountId,Account.EnvironmentalAccountType__c from User where id=:userinfo.getuserId()];
if(u.Account.EnvironmentalAccountType__c == 'Distributor')
          {
           isDistributor = true;
          }
          else if(u.Account.EnvironmentalAccountType__c == 'Contractor')
          {
           isDistributor = false;
          }
          return isDistributor;
}
set;}

  public LCBS_Building_MoreInfo_BuildInfo_Con(ApexPages.StandardController controller) {
      currentuser = [select id,name,contact.Role__c from user where id=:userinfo.getuserId()];
      buildingId = System.currentPageReference().getParameters().get('Id');
       b = [select id,name,CreatedBy.id,Owner_Approved__c,Owner_Rejected__c,OwnerId,Building_Owner__c from Building__c where id=:buildingId];
       c = [select id,Contact.Account.id from user where id=:b.Createdby.id];
      if(b.Building_Owner__c!=NULL)
      {
       buildingowner = true;
       buildOwnerStatusPanel = true;
       system.debug('chiran11**********');
       build = [select id,name,Address__c,City__c,Email_Address__c,Country__c,Postal_Code__c,Phone_Number__c,State__c from Building_Owner__c where id=:b.Building_Owner__c]; 
      }
      else
      {
      buildingowner = false;
      addBuildPanel = true;
      system.debug('chiran22**********');
      }
      system.debug('b.Owner_Approved__c+++'+b.Owner_Approved__c);
//      if(b.Owner_Approved__c==true){
//          AddBuildingCallout.geBuildings(b.name,string.valueof(b.Id));
//      }
 PAGE_CSS = LCBSParameters.PAGE_CSS_URL;
     if(b.Owner_Rejected__c == false && b.Owner_Approved__c==false)
     {
      buildingOwnerStatus = 'Invited';
     }
     else if(b.Owner_Rejected__c == false && b.Owner_Approved__c==true)
     {
      buildingOwnerStatus = 'Accepted';
     }
     else if(b.Owner_Rejected__c == true && b.Owner_Approved__c==false)
     {
      buildingOwnerStatus = 'Rejected';
     }
    }
public LCBS_Building_MoreInfo_BuildInfo_Con(){
 
 
  
  
} 
public void sendEmail(){
        try{
         EmailTemplate myEMailTemplate = [Select id,name from EmailTemplate where Name ='LCBS_BuildingAccessReqTemplate'];
         mails = new List<Messaging.SingleEmailMessage>();
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
            //Contact c = [select id, Email from Contact where email <> null limit 1];
           User tempUser =[select id,Contactid from User where id=:userinfo.getuserId()];
            mail.setTemplateId(myEMailTemplate.id);
            List<String> sendTo = new List<String>();
            sendTo.add(build.Email_Address__c );
            system.debug('Email address###:'+build.Email_Address__c);
            mail.setToAddresses(sendTo);
            mail.setTargetObjectId(tempUser.contactid);           
            mail.setSenderDisplayName('LCBS Building Approval');
            //mail.setReplyTo('approval_for_building@1lpcig5s4p7ux2ltna158pjo0mknx7t77fw3y7pndbub6bpijt.g-3nq6jeac.cs17.apex.sandbox.salesforce.com');
            //string emailMessage ='Your heating and cooling contractor is requesting data access to your building'+ ' '+build.Name+'.  To grant them access, please reply "Approved" to this mail. To reject them access, please reply "Reject" to this mail <br/><br/><br/> Thank you.';
            system.debug('Building ID ^^^^^^^:'+build.id);
            mail.whatid = build.id; 
            mails.add(mail);
            system.debug('mails ########:'+mails);
            Savepoint sp = Database.setSavepoint();
            system.debug('mails ^^^^^^^:'+mails);
            if(mails.size()>0){
            Messaging.sendEmail(mails);
            Database.rollback(sp);
            
             List<Messaging.SingleEmailMessage> lstMsgsToSend = new List<Messaging.SingleEmailMessage>();
             for (Messaging.SingleEmailMessage email : mails) {
             system.debug('inside the for loop');
             Messaging.SingleEmailMessage emailToSend = new Messaging.SingleEmailMessage();
             emailToSend.setToAddresses(email.getToAddresses());
             emailToSend.setPlainTextBody(email.getPlainTextBody());
             emailToSend.setHTMLBody(email.getHTMLBody());
             emailToSend.setSubject(email.getSubject());
             lstMsgsToSend.add(emailToSend);
             }
              Messaging.sendEmail(lstMsgsToSend);
            }
            }
            catch(Exception e)
            {
             system.debug(e);
            }
       }     
public pagereference equipmentPage()
{    
 //String equipment = 'tag:honeywell.com,2012:acs/system#_'+buildingId;
 //String equipment = buildingId;
 //Blob equipmentBlob = Blob.valueOf(equipment);
 //string convertedEquipment= EncodingUtil.base64Encode(equipmentBlob);
 PageReference p = new PageReference(LCBSParameters.Domain_URL+'/app/equipments/'+c.Contact.Account.id+'/'+buildingId);
 p.setRedirect(true);
 return p; 
}

public pagereference alertsPage()
{    
 //String equipment = 'tag:honeywell.com,2012:acs/system#_'+buildingId;
 //String equipment = buildingId;
 //Blob equipmentBlob = Blob.valueOf(equipment);
 // string convertedEquipment= EncodingUtil.base64Encode(equipmentBlob);
 PageReference p = new PageReference(LCBSParameters.Domain_URL+'/app/alerts/'+c.Contact.Account.id+'/'+buildingId);
 p.setRedirect(true);
 return p; 
}
public pagereference controlsPage()
{    
// String equipment = 'tag:honeywell.com,2012:acs/system#_'+buildingId;
 //String equipment = buildingId;
 //Blob equipmentBlob = Blob.valueOf(equipment);
 //string convertedEquipment= EncodingUtil.base64Encode(equipmentBlob);
 PageReference p = new PageReference(LCBSParameters.Domain_URL+'/app/controls/'+c.Contact.Account.id+'/'+buildingId);
 p.setRedirect(true);
 return p; 
}
public pageReference addBuildOwner()
{
  pageReference p = new  pageReference('/LCBSBuildingOwner?Id='+buildingId);
  p.setRedirect(true);
  return p;
}

}