/**
 * An apex page controller that takes the user to the right start page based on credentials or lack thereof
 */
public with sharing class CommunitiesLandingController {
    
    // Code we will invoke on page load.
    public PageReference forwardToStartPage() {

/*        String CommunityId = Network.getNetworkId();
        System.debug('CommunityId:::'+CommunityId);
        
        String CommunityUrl = Network.getLoginUrl(CommunityId);
        System.debug('CommunityUrl:::'+CommunityUrl);
        
        String Env = CommunityUrl.remove('https://').substringBefore <https://').substringbefore/> ('-');
        System.debug('Environment:::'+Env);
       
        LCBS_AzureUrls__c homepage = LCBS_AzureUrls__c.getValues(Env);
        System.debug('homepage:::'+homepage);
        
        return new PageReference(homepage.LCBS_HomeUrl__c); */
        
        User u = [Select id, FederationIdentifier, Contact.Name, Account.EnvironmentalAccountType__c from user where id=:userinfo.getUserId()];
        system.debug('$$$$$$$$' + u.FederationIdentifier);
        system.debug('######## Naga Test:Before Record Type IF');
        if(u.Account.EnvironmentalAccountType__c =='Contractor')
        {
           system.debug('######## Naga Test:Record Type matched in IF(Contractor)');
            pageReference p = new pageReference(LCBSParameters.DISTRIBUTOR_HOME_URL);
           p.setRedirect(true);
           return p;
        }
        else if(u.Account.EnvironmentalAccountType__c == 'Distributor')
        {
           system.debug('######## Naga Test:Record Type matched in IF(Distributor)');
           pageReference p = new pageReference('/LCBSContractorPage');
           p.setRedirect(true);
           return p;
        } 
        
        return Network.communitiesLanding();
    }
    
    //Added By Nagarajan
   /*  public PageReference forwardToCustomStartPage() {
        return Page.LCBS_ContractorOwnerDashboard;
        
    }*/
    
    public CommunitiesLandingController() {}
}