@isTest(seeAllData = false)
public class LCBS_BuildingApproveReject_Test
{
    static Contact contact;
    static Building__c build, build1, build2;
    
    static testMethod void myTest(){
        
        build = LCBS_TestDataFactory.createBuilding(1)[0];
        contact=LCBS_TestDataFactory.createContact(3)[0];
     
        PageReference pageRef1 = Page.LCBS_BuildingApproveReject;
        Test.setCurrentPageReference(pageRef1);
        ApexPages.currentPage().getParameters().put('id', build.id);
        Id currentRecordId =build.id;
        
        Building__C b=new Building__c();
        b.Owner_Approved__c = true;
        insert b;
                
        LCBS_BuildingApproveReject_Controller barc = new LCBS_BuildingApproveReject_Controller();
        barc.Approved=True;
        barc.approveAction();
        
        List<Apexpages.Message> pageMessages = ApexPages.getMessages();
        
        }
    static testmethod void testapproveflag() {
    
        build1 = LCBS_TestDataFactory.createBuilding(1)[0];
        
        PageReference PageRef2 = Page.LCBS_BuildingApproveReject;
        Test.setCurrentPageReference(pageRef2);
        ApexPages.currentPage().getParameters().put('id', build1.id);
        Id currentRecordId =build1.id;
        
        LCBS_BuildingApproveReject_Controller brc = new LCBS_BuildingApproveReject_Controller();
        brc.fetchinfo.Owner_Approved__c = true;
        boolean app = brc.Approved;
        brc.fetchinfo.Owner_Approved__c = false;
        boolean app1 = brc.Approved;
    }
    
    static testmethod void testcatchblock() {
        
        build2 = LCBS_TestDataFactory.createBuilding(1)[0];
        
        PageReference PageRef3 = Page.LCBS_BuildingApproveReject;
        Test.setCurrentPageReference(pageRef3);
        ApexPages.currentPage().getParameters().put('id', build2.id);
        Id currentRecordId =build2.id;
        
        LCBS_BuildingApproveReject_Controller brc1 = new LCBS_BuildingApproveReject_Controller();
//        brc1.fetchinfo.Owner_Approved_date__c = ;
        delete brc1.fetchinfo;
        brc1.approveAction();
    }
}