Trigger LCBS_BuildingOwnerTrigger on Building_Owner__c (Before Insert, Before Update, Before Delete,After Insert, After Update, After delete,After Undelete) {
/*****/
    if(Trigger.isBefore && Trigger.isInsert) {
        //Handler
    } else if(Trigger.isBefore && Trigger.isUpdate) {
        // Handler
    } else if(Trigger.isBefore && Trigger.isDelete) {
        // Handler
    } else if(Trigger.isAfter && Trigger.isInsert) {
        // Handler
    } else if(Trigger.isAfter && Trigger.isUpdate) {
        // Handler
    } else if(Trigger.isAfter && Trigger.isDelete) {
        // Handler
    } else if(Trigger.isAfter && Trigger.isUndelete) {
        // Handler
    }
/*****/
}